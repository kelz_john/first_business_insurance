import { AppRegistry } from 'react-native';
import App from './application/app';

AppRegistry.registerComponent('first_business_insurance', () => App);
