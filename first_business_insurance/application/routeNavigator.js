import React, { Component } from 'react';
import { AppRegistry,View,Text,StyleSheet,ScrollView,TouchableOpacity } from 'react-native';
import { StackNavigator, DrawerNavigator  } from 'react-navigation';

import HomeScreen from './home';
import HomeScreen2 from './home_er';
import LoginScreen from './Login';
import QuoteScreen from './quote';
import ClaimsScreen from './claims';

const MainView = DrawerNavigator({
	  Home: { screen: HomeScreen },
	  Quote: { screen: QuoteScreen},
	Claims: { screen: ClaimsScreen}
	//  UserProfile: {screen: UserProfileScreen },
	//  BursaryHome: {screen:BursaryHomeScreen},
	//  UserBursaryRegistration: {screen:UserBursaryRegistrationScreen},
	//  EntityBursaryRegInfoProcess: {screen:EntityBursaryRegInfoProcessScreen}
},
{
	initialRouteName : 'Home',
	drawerPosition:'left',	
	swipeEnabled:false,
	drawerLockMode:'locked-closed',
	contentOptions:{
		activeBackgroundColor: '#00508f'
	},	
	//contentComponent: props => <Menu {...props}  />
});

const firstBusinessInsuranceAppNav = StackNavigator({
	 Main: { screen: MainView },	
    Login: { screen: LoginScreen},
    // ForgotPassword: {screen:ForgotPasswordScreen },
    // ForgotPasswordSuccess: {screen:ForgotPasswordSuccessScreen},
	// Register: {screen:RegisterScreen},
	
 },{
	initialRouteName : 'Login',
	headerMode : 'none',
	navigationOptions: {
		gesturesEnabled: false
	  }
 });
 
export default firstBusinessInsuranceAppNav;