import {TAB_SELECT} from './types';

export const OnTabSelection = (selectedTabItem) => {
    console.log('actioned'+JSON.stringify(selectedTabItem))
    return {
        type: TAB_SELECT,
        payload: selectedTabItem
    }
}