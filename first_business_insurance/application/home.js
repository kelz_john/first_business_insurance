/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, TouchableOpacity, Image, Dimensions, ScrollView
} from 'react-native';
import FooterMenu from './footer';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
import {
    Drawer,
    Button,
    Title,
    Container,
    Toolbar,
    Segment,
    Header,
    Footer,
    FooterTab,
    Badge,
    Content,
    Icon,
    Right,
    Label,
    Form,
    Item,
    List,
    ListItem,
    Input,
    Card,
    CardItem,
    Left,
    Thumbnail,
    Radio,
    Body
  } from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import Swiper from 'react-native-swiper';
const xWidth = Dimensions.get('window').width;
const xHeight = Dimensions.get('window').height;
export default class home extends React.Component {
  render() {

    const gradientHeight=400
      const gradientBackground = '#0095B6';
 const data = Array.from({ length: gradientHeight });
    return (
    <Container>
            <Header style={{ backgroundColor: '#0f1225' }}>
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ marginTop: 10, color: 'white', fontSize: 24, fontFamily: 'roboto' }}>
                        Home
                    </Text>
                </View>
            </Header>
            {/* <Icon name={"ios-log-out"} style={{ color: 'white', alignSelf: 'center', padding: 40,marginTop:30, position:'absolute' , zIndex:11111}} /> */}
            <Content style={{ backgroundColor: '#0095B6'}}>
               
      <View style={{flex:1}}>
     
                {data.map((_, i) => (
                    <View
                        key={i}
                        style={{
                            position: 'absolute',
                            backgroundColor: gradientBackground,
                           // height: 1,
                           // top: (gradientHeight - i),
                           // right: 0,
                           // left: 0,
                           // zIndex: -2,
                          //  opacity: (1 / gradientHeight) * (i + 1)
                        }}
                    >
                  
                    </View>
                ))} 
           
           <View style={{
                        height: 100,
                        width: 100,
                        borderRadius: 50,
                        borderColor: '#0f1225',
                        borderWidth: 2,
                        alignSelf:'flex-start',
                        justifyContent: 'center',
                        marginLeft:20,
                        marginTop: 20
                    }}>
                        <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 38, color: '#0f1225' }}>
                            F.B
                        </Text>

            </View>

                        <View style={styles.slide1}>
                            <Text style={{color:'white',fontFamily:'roboto' ,fontSize:32, fontWeight:'bold'}}>
                            First Business
                            </Text>
                        <Text style={{ color:'#0f1225', fontSize:28, marginTop:10}}>
                            The smart business insurance solution
                            </Text>
                            <Text style={{color:'black', fontSize:18, marginTop:10, opacity:0.5}}>
                            Insuring your business assets are essential, but that doesn’t mean insurance has to	be complicated and time-consuming.	
                            </Text>
                        <Text style={{ color: 'black', fontSize: 18, marginTop: 10, opacity: 0.5}}>
                            That’s why we offer	you	First Business – a simple and comprehensive	solution to	your business’s	needs.	
                            </Text>
                        </View>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate('Quote')}>
                    <View style={{
                        height: 100,
                        width: 100,
                        borderRadius: 50,
                        borderColor: '#0f1225',
                        borderWidth: 2,
                        alignSelf: 'flex-end',
                        justifyContent: 'center',
                        marginLeft: 20,
                        marginRight:30,
                        flexDirection:'row',
                        backgroundColor:'#0f1225',
                        alignItems:'center'
                        //marginTop: 20
                    }}>

                        <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 19, color: 'white' }}>
                           Next
                        </Text>
                        <Icon name="ios-arrow-forward" style={{color:'white', marginLeft:10}}/>
                    </View>
                    </TouchableOpacity>





            </View>
            
         
    </Content>
      <FooterMenu ActiveTab="Home" nav={this.props.navigation}/>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },buttonContainer: {
    width: 200,
    alignItems: 'center',
},wrapper:{
position:'relative',
height:xHeight*0.85
//marginTop:-90

},
buttonText: {
    textAlign: 'center',
    color: '#4C64FF',
    padding: 15,
    width: 200
},
slide1: {
    flex: 1,
    //justifyContent: 'center',
   // padding:30,
    marginTop:10,
    marginBottom:10,
    marginLeft:20,
    width:xWidth*0.9
    //zIndex:1111111,
//alignItems: 'center',
 //backgroundColor: 'white',
  },
  
});
