import { combineReducers } from 'redux';
import TabReducer from './tabReducer';

const rootReducer = combineReducers({
    tab: TabReducer,
});

export default rootReducer;