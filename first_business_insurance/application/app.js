import React from 'react';
import Routes from './routeNavigator';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './reducers';

let store = createStore(reducers);
const App = () => {
	return(
		<Provider store={store}>
			<Routes />
		</Provider>
	)
}

export default App;