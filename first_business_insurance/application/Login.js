/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, TouchableOpacity
} from 'react-native';
import FooterMenu from './footer';
import { Container, Content, Input, Item } from 'native-base';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


export default class home extends React.Component {
  render() {
    return (
     <Container>
        <Content style={{ backgroundColor:'#0f1225'}}> 
                <View style={{height:100, 
                              width:100, 
                              borderRadius:50, 
                              borderColor:'white', 
                              borderWidth:1, 
                              alignSelf:'center', 
                              justifyContent:'center',
                            
                              marginTop:50}}>
                                <Text style={{textAlign:'center', fontWeight:'bold', fontSize:38, color:'white'}}>
                                  F.B
                                </Text>

                </View>
                <Text style={{textAlign:'center', fontSize:20, color:'white', marginTop:10}}>
                                  First Business
                                </Text>
                                <Text style={{textAlign:'center', fontSize:20, color:'white', marginTop:10}}>
                                  Insurance
                                </Text>
        <View style={{padding:20, marginTop:20}}>
          <Item regular >
            <Input placeholder='USERNAME' placeholderTextColor={'white'} style={{color:'white'}} />
          </Item>
        </View>

        <View style={{padding:20}}>
          <Item regular >
            <Input placeholder='PASSWORD' placeholderTextColor={'white'} style={{color:'white'}} />
          </Item>
        </View>

          <TouchableOpacity onPress={() => { this.props.navigation.navigate('Home') }}>
                <View style={{height:40, 
                              width:300, 
                              alignSelf:'center', 
                              justifyContent:'center',
                                backgroundColor:'#7EF9FF',
                              marginTop:30}}>
                                <Text style={{textAlign:'center', fontSize:18, color:'black'}}>
                                 LOG IN
                                </Text>

                </View>
                </TouchableOpacity>

            <TouchableOpacity>
                <Text style={{textAlign:'center', fontSize:18, color:'#7EF9FF', marginTop:30}}>
                                 FORGOT PASSWORD?
                </Text>
             </TouchableOpacity>

             <TouchableOpacity>
                <Text style={{textAlign:'center', fontSize:18, color:'white', marginTop:40}}>
                                 NEW ACCOUNT
                </Text>
                <View style={{borderColor:'white', borderWidth:1, borderRadius:1, alignSelf:'center', width:130}}/>
             </TouchableOpacity>
         </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
