/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, Dimensions, ScrollView, TextInput
} from 'react-native';
import FooterMenu from './footer';
const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' +
        'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
import {
    Drawer,
    Button,
    Title,
    Container,
    Toolbar,
    Segment,
    Header,
    Footer,
    FooterTab,
    Badge,
    Content,
    Icon,
    Right,
    Label,
    Form,
    Item,
    List,
    ListItem,
    Input,
    Card,
    CardItem,
    Left,
    Thumbnail,
    Radio,
    Body
} from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import Swiper from 'react-native-swiper';
const xWidth = Dimensions.get('window').width;
const xHeight = Dimensions.get('window').height;

export default class quote extends React.Component {
    render() {

        const gradientHeight = 400
        const gradientBackground = '#0095B6';
        const data = Array.from({ length: gradientHeight });
        return (
            <Container>
                <Header style={{ backgroundColor: '#0095B6' }}>
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ marginTop: 10, color: 'white', fontSize: 24, fontFamily: 'roboto' }}>
                            Quote
                    </Text>
                    </View>
                </Header>
                {/* <Icon name={"ios-log-out"} style={{ color: 'white', alignSelf: 'center', padding: 40,marginTop:30, position:'absolute' , zIndex:11111}} /> */}
                <Content style={{ backgroundColor: '#0f1225' }}>

                    <View style={{ flex: 1 }}>

                        {data.map((_, i) => (
                            <View
                                key={i}
                                style={{
                                    position: 'absolute',
                                    backgroundColor: gradientBackground,
                                    // height: 1,
                                    // top: (gradientHeight - i),
                                    // right: 0,
                                    // left: 0,
                                    // zIndex: -2,
                                    //  opacity: (1 / gradientHeight) * (i + 1)
                                }}
                            >

                            </View>
                        ))}

                        {/* <View style={{
                            height: 100,
                            width: 100,
                            borderRadius: 50,
                            borderColor: 'white',
                            borderWidth: 2,
                            alignSelf: 'flex-start',
                            justifyContent: 'center',
                            marginLeft: 20,
                            marginTop: 20
                        }}>
                            <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 38, color: 'white' }}>
                                F.B
                            </Text>

                        </View> */}

                        <View style={styles.slide1}>
                            <View style={{marginLeft:20,  marginTop: 20 }}>
                                <Item regular >
                                    <Input placeholder='Select type of insurance' placeholderTextColor={'white'} style={{ color: 'white' }} />
                                    <Icon name="ios-arrow-down" style={{color:'white'}}/>               
                                    </Item>
                            </View>
                            <View style={{ marginLeft: 20, marginTop: 20 }}>
                                <Item floatingLabel>
                                    <Label style={{color:'white'}}>Enter your ID Number</Label>
                                    <Input />
                                </Item>

                            </View>
                            <View style={{ marginLeft: 20, marginTop: 20 }}>
                                <Item regular >
                                    <Input placeholder='Select your Language' placeholderTextColor={'white'} style={{ color: 'white' }} />
                                    <Icon name="ios-arrow-down" style={{ color: 'white' }} />
                                </Item>
                            </View>
                            <View style={{ marginLeft: 20, marginTop: 20 }}>
                                <Item regular >
                                    <Input placeholder='Select your Gender' placeholderTextColor={'white'} style={{ color: 'white' }} />
                                    <Icon name="ios-arrow-down" style={{ color: 'white' }} />
                                </Item>
                            </View>
                            
                            <View style={{ marginLeft: 20, marginTop: 20 }}>
                                <Text style={{ color: 'white', marginBottom:10 }}>Items you want covered </Text>
                                <View style={{ height:60, borderWidth:1, borderColor:'white'}}>
                                <Item>
                                   {/* <TextInput multiline={true} /> */}
                                    </Item>
                               </View>
                            </View>
                          
                        </View>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('') }}>
                            <View style={{
                                height: 40,
                                width: 300,
                                alignSelf: 'center',
                                justifyContent: 'center',
                                backgroundColor: '#0095B6',
                                marginTop: 30
                            }}>
                                <Text style={{ textAlign: 'center', fontSize: 17, color: 'white' }}>
                                SUBMIT YOUR REQUEST
                                </Text>

                            </View>
                        </TouchableOpacity>

                    </View>
                </Content>
                <FooterMenu ActiveTab="Quote" nav={this.props.navigation} />
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    }, buttonContainer: {
        width: 200,
        alignItems: 'center',
    }, wrapper: {
        position: 'relative',
        height: xHeight * 0.85
        //marginTop:-90

    },
    buttonText: {
        textAlign: 'center',
        color: '#4C64FF',
        padding: 15,
        width: 200
    },
    slide1: {
        flex: 1,
        //justifyContent: 'center',
        // padding:30,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        width: xWidth * 0.9
        //zIndex:1111111,
        //alignItems: 'center',
        //backgroundColor: 'white',
    },

});
