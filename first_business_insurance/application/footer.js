import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  WebView,
  StyleSheet,
  Dimensions,
  Select,
  View,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView,
  TouchableWithoutFeedback,
  Image,
  AsyncStorage, 
  Platform
} from 'react-native'
import {
  Drawer,
  Button,
  Title,
  Container,
  Toolbar,
  Segment,
  Text,
  Header,
  Footer,
  FooterTab,
  Badge,
  Content,
  Icon,
  Right,
  Label,
  Form,
  Item,
  List,
  ListItem,
  Input,
  Card,
  CardItem,
  Left,
  Thumbnail,
  Radio,
  Body
} from 'native-base'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import {OnTabSelection} from '../application/actions';

const xWidth = Dimensions.get('window').width
let xHeight = Dimensions.get('window').height

class FooterMenu extends Component {
  constructor(props){
    super(props);
  }
 tabSelected(name){
    console.log(this.props.selectedTabItem);
    let Arrray = this.props.selectedTabItem;
    for(i=0; i< Arrray.length; i++){
      Arrray[i].active = false;
      if(Arrray[i].name === name){
        Arrray[i].active = true;
      }
    }
    console.log(Arrray);
    this.props.OnTabSelection(Arrray);
    
  }
  componentWillMount(){
    console.log(this.props.ActiveTab);
    this.tabSelected(this.props.ActiveTab);
  }
 
  render(){
  return (
 
          <View style={{ backgroundColor: '#f5f5f5' , 
          flexDirection:'row', height:70, width:xWidth, borderTopWidth:0.5}}>
      <TouchableOpacity
        vertical
        onPress={() => {this.props.nav.navigate('Home')}}
         onPressOut={() => this.tabSelected("Home")}
        active={this.props.selectedTabItem[0].active}
        style={{
          borderColor: 'grey', borderWidth: 0.1,
          //  backgroundColor: this.props.selectedTabItem[1].active? '#00508f':'transparent'
        }}
      >
        {/* <Image
                style={{ width: 21, height: 21, marginTop: 5 }}
                source={this.props.selectedTabItem[1].active?require('../../images/White_FixErrors.png'):require('../../images/blue_fixerrors.png')}
                resizeMode="contain"
              /> */}
        <View style={{ justifyContent: 'center', width: xWidth * 0.2, marginTop: 10 }}>
          <Icon name="home" style={{ color: this.props.selectedTabItem[0].active ? '#0095B6' : 'black', textAlign: 'center' }} />
          <Text
            style={{
              textAlign: 'center',
              fontSize: 9,
              color: this.props.selectedTabItem[0].active ? '#0095B6' : 'black',
              //width:Platform.OS === 'ios' ? 70 :xWidth*0.23,// 67,
            }}
          >
            HOME
              </Text>
        </View>
      </TouchableOpacity>
      
      <TouchableOpacity
        vertical
        //onPress={() => this.props.nav.navigate('FixErrors')}
       // onPressOut={() => tabSelected("FixErrors")}
        active={this.props.selectedTabItem[1].active}
        style={{
          borderColor: 'grey', borderWidth: 0.1,
          // backgroundColor: this.props.selectedTabItem[1].active? '#00508f':'transparent'
        }}
      >
        {/* <Image
                style={{ width: 21, height: 21, marginTop: 5 }}
                source={this.props.selectedTabItem[1].active?require('../../images/White_FixErrors.png'):require('../../images/blue_fixerrors.png')}
                resizeMode="contain"
              /> */}
        <View style={{ justifyContent: 'center', width: xWidth * 0.2, marginTop: 10 }}>
          <Icon name="analytics" style={{ color: this.props.selectedTabItem[1].active ? '#0095B6' : 'black', textAlign: 'center' }} />
          <Text
            style={{
              textAlign: 'center',
              fontSize: 9,
              color: this.props.selectedTabItem[1].active ? '#0095B6' : 'black',
              //width:Platform.OS === 'ios' ? 70 :xWidth*0.23,// 67,
            }}
          >
            ADJUST
              </Text>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 9,
              color: this.props.selectedTabItem[1].active ? '#0095B6' : 'black',
              //width:Platform.OS === 'ios' ? 70 :xWidth*0.23,// 67,
            }}
          >
            COVER
              </Text>
        </View>
      </TouchableOpacity>
            <TouchableOpacity
              vertical
          
              onPressOut={() => this.tabSelected("Quote")}
              onPress={() => this.props.nav.navigate('Quote')}
              active={this.props.selectedTabItem[2].active}
              style={{ borderColor: 'grey', borderWidth: 0.1, 
             // backgroundColor: this.props.selectedTabItem[0].active? '#00508f':'transparent' 
            }}
            >
              {/* <Image
                style={{ width: 21, height: 21, marginTop: 5 }}
                source={this.props.selectedTabItem[0].active?require('../../images/White_Accountstatus.png'):require('../../images/blue_Accountstatus.png')}
                resizeMode="contain"
              /> */}
              <View style={{justifyContent:'center', width:xWidth*0.2, marginTop:10}}>
          <Icon name="create" style={{ color: this.props.selectedTabItem[2].active ? '#0095B6':'black', textAlign:'center' }} />
              <Text
            style={{ textAlign: 'center', fontSize: 9, color: this.props.selectedTabItem[2].active ? '#0095B6':'black' }}
              >
                QUOTE
              </Text>
              </View>
            </TouchableOpacity>
           
            
            <TouchableOpacity
              vertical
               onPress={() => this.props.nav.navigate('Claims')}
              active={this.props.selectedTabItem[3].active}
              style={{ borderColor: 'grey', borderWidth: 0.1, 
              //backgroundColor: this.props.selectedTabItem[2].active? '#00508f':'transparent' 
            }}
        
            >
              {/* <Image
                style={{ width: 21, height: 21, marginTop: 5 }}
                source={this.props.selectedTabItem[2].active?require('../../images/White_PaymentHistory.png'):require('../../images/blue_PaymentHistory.png')}
                resizeMode="contain"
              /> */}
              <View style={{justifyContent:'center', width:xWidth*0.2, marginTop:10}}>
               <Icon name="albums" style={{
            color: this.props.selectedTabItem[3].active ? '#0095B6':'black',
                   textAlign:'center'}} />
              <Text
            style={{ textAlign: 'center', fontSize: 9, 
                      color: this.props.selectedTabItem[3].active ? '#0095B6':'black' }}
              >
               CLAIM
              </Text>
              </View>
            </TouchableOpacity>
         
         
             <TouchableOpacity              
              vertical
             // onPress={() => this.props.nav.navigate('ChangePackage')}
              active={this.props.selectedTabItem[4].active}
              style={{ borderColor: 'grey', borderWidth: 0.1, 
            //  backgroundColor: this.props.selectedTabItem[3].active? '#00508f':'transparent' 
            }}
            >
             <View style={{justifyContent:'center', width:xWidth*0.2, marginTop:10}}>
          <Icon name="call" style={{ color: this.props.selectedTabItem[4].active ? '#0095B6':'black', 
            textAlign:'center'}} />
              <Text
            style={{ textAlign: 'center', fontSize: 9, color: this.props.selectedTabItem[4].active ? '#0095B6':'black' }}
              >
               CONTACT ME
              </Text>
              </View>
            </TouchableOpacity> 
      
           
            
          </View>
     
  )
}
}

FooterMenu.propTypes = {
    ActiveTab: PropTypes.string,
    nav: PropTypes.object
}

const mapStateToProps = state =>{
  return {
    selectedTabItem: state.tab.selectedTabItem
  };
}
const mapDispatchToProps = dispatch =>{
  return bindActionCreators({ OnTabSelection: OnTabSelection}, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(FooterMenu);